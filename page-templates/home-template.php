<?php
//Template Name: Početna

    get_header();
    if(have_posts()) : while(have_posts()) : the_post();
?>
    <!-- SECTION 1 -->
    <div class="container-fluid home_hero_fluid" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
        <div class="overlay-div"></div>
        <div class="row">
            <div class="col-12 title-subtitle-wrap">
                <div class="hero-title-wrapper">
                    <h1>ОПШТИНА <br>ЗВЕЗДАРА</h1>
                </div>
                <div class="hero-subtitle-wrapper">
                    <h2>У служби грађана</h2>
                </div>
            </div>
            <div class="col-12">
                <div class="hero-search-wrap">
                    <?php get_search_form(); ?>
                    <p id="ss"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 1 END-->

    <!-- SECTION 2 -->
    <div class="container-fluid home-news-fluid res-padding" id="vesti">
        <div class="row">
            <div class="col-12">
                <div class="home-news-links-wrapper">
                    <ul class="home-news-links-list">
                        <?php 
                        $home_cat_links = get_field('kategorie_vesti');
                            foreach($home_cat_links as $hcl) { ?>
                            <li><a href="<?php echo get_category_link($hcl); ?>"><?php echo $hcl->name; ?></a></li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
            </div>
           <div class="col-12">
                <div class="home-news-posts-wrapper">
                    <div class="swiper-container" style="width: 100%;">
                        <div class="swiper-wrapper">
                            <?php
                                $slider_cat = get_field('kategorija_za_slajder');
                                $args = array(
                                    'category' => $slider_cat,
                                    'numberposts' => 20
                                );
                                $slider_posts = get_posts($args);
                    
                                foreach($slider_posts as $sp) { ?>
                                <div class="swiper-slide">
                                    <div class="slider-post-wrapper">
                                        <div class="slider-post-image">
                                            <?php echo get_the_post_thumbnail($sp, 'posts-size'); ?>
                                        </div>
                                        <div class="post-box-contnet-wrapper">
                                            <div class="slider-post-date">
                                                <?php echo get_the_date('d.m.Y', $sp); ?>
                                            </div>
                                            <div class="slider-post-title">
                                                <h3><?php echo $sp->post_title ?></h3>
                                            </div>
                                            <div class="slider-post-button">
                                                <a href="<?php echo get_permalink($sp) ?>">Читајте даље</a>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                        <!-- Add Arrows --> 
                    </div>
                    <div class="swiper-button-prev-unique ar-wrap">
                        <div class="arrow-slider-wrapper">
                            <img src="<?php echo get_template_directory_uri()?>/images/left-hslider.png">
                        </div>
                    </div>
                    <div class="swiper-button-next-unique ar-wrap">
                        <div class="arrow-slider-wrapper">
                            <img src="<?php echo get_template_directory_uri()?>/images/right-hslider.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 2 END-->

    <!-- SECTION 3 START-->
    <div class="container-fluid fast-links-fluid" id="brzi-linkovi">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-md-12 col-lg-4 fast-links-l-col">
                        <div class="fast-links-big-title res_title">
                            <h2><?php the_field('brzi_linkovi_naslov'); ?></h2>
                        </div>
                        <div class="fl_devider"></div>
                        <div class="fast-links-description">
                            <p><?php the_field('brzi_linkovi_tekst'); ?></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8"> 
                        <div class="row">
                            <?php
                                $fast_links = get_field('brzi_linkovi');
                                foreach($fast_links as $fs) { ?>
                                    <div class="col-6 col-lg-6 col-xl-3 fs_col">
                                        <a href="<?php echo $fs['link']; ?>" class="fl_wrapper">
                                            <h3><?php echo $fs['naslov']; ?></h3>
                                            <div class="fl_img_wrapper">
                                                <img src="<?php echo $fs['slika']; ?>" >
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 3 END-->

    <!-- SECTION 4 START-->
    <div class="container-fluid home-baners-fluid res-padding" id="baneri">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-12 baners-title-col">
                        <p>Банери</p>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <?php 
                                $baners = get_field('baneri');
                                foreach($baners as $hb) { ?> 
                                     <div class="col-md-12 col-lg-6 col-xl-3 baner-col">
                                        <div class="single_baner_wrapper">
                                            <?php if($hb['slika_banera']) : ?>
                                            <div class="sb_img_wrapper">
                                                <img src="<?php echo $hb['slika_banera']; ?>" >
                                            </div>
                                            <?php endif; ?>
                                            <div class="sb_content_wrapper">
                                                <h3 class="sb_title">
                                                    <?php echo $hb['naslov_banera']; ?>
                                                </h3>
                                                <div class="sb_devider"></div>
                                                <a class="sb_link" href="<?php echo $hb['link_banera']; ?>"><span>Сазнај више</span></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            ?>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>                        
    <!-- SECTION 4 END-->
    
    <!-- SECTION 5 -->
    <div class="container-fluid home-eservis-fluid res-padding" id="euprava">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-12 eservice-title-col">
                        <p>еУправа</p>
                    </div>
                    <div class="col-md-6">
                        <div class="eservice-left-title-wrapper res_title">
                            <h2><?php the_field('e_uprava_naslov'); ?></h2>
                            <img src="/wp-content/uploads/2020/12/e-uprava-logo.png">
                        </div>
                        <div class="eservice_devider"></div>
                        <div class="eservice_content_link">
                            <p><?php the_field('e_uprava_tekst'); ?></p>
                            <a href="<?php the_field('e_uprava_link_za_dugme'); ?>">Наручивање докумената</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right-col-services-wrapper">
                            <div class="services-right-title-wrapper res_title">
                                <h2><?php the_field('opstinski_servisi_naslov'); ?></h2>
                            </div>
                            <div class="row">
                                <?php
                                    $service = get_field('opstinski_servisi');
                                    foreach($service as $fl) { ?>
                                        <div class="col-md-6">
                                            <a class="s-service-wrapper" href="<?php echo $fl['link_do_servisa']; ?>">
                                                <div class="service_icon-wrapper">
                                                    <img src="<?php echo $fl['slika_servisa']; ?>">
                                                </div>
                                                <h3><?php echo $fl['naslov_servisa']; ?></h3>
                                            </a>
                                        </div>
                                    <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 5 END-->
    
    <!-- SECTION 6-->
    <div class="container-fluid home-sugestion-fluid res-padding" id="gradjani-predlazu">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-12 sugestion-title-col">
                        <p>Грађани предлажу</p>
                    </div>
                    <div class="col-md-6 lef-main-half-col">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="left-sep_post">
                                    <?php
                                        $separate_post = get_field('izdvojen_post');
                                    ?>
                                    <p class="sep_post_date"><?php echo get_the_date('d.m.Y', $separate_post); ?></p> 
                                    <p class="sep_post_content"><?php echo get_the_excerpt($separate_post); ?></p>     
                                    <div class="slider-post-button">
                                        <a href="<?php echo get_permalink($separate_post); ?>">Читајте даље</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="separate_post_image">
                                    <img src="<?php echo get_the_post_thumbnail_url($separate_post); ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="people_sugest_wrapper res_title">
                            <h2><?php the_field('gradjani_predlazu_naslov') ?></h2>
                            <div class="border_wrapper">
                            <p class="devider-p "><?php the_field('gradani_predlazu_tekst') ?></p>
                            <div class="row sugestion_boxes_row">
                            <div class="col-md-6">
                                <div class="sugestion_bexes">
                                    <p><?php the_field('gp_text_1') ?></p>
                                    <a href="<?php the_field('gp_link_1') ?>"></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="sugestion_bexes">
                                    <p><?php the_field('gp_text_2') ?> </p>
                                    <a href="<?php the_field('gp_link_2') ?>"></a>
                                </div>
                            </div>
                        </div>
                            </div>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 6 END-->

    <!-- SECTION 7 -->
    <div class="container-fluid home-green-fluid res-padding" id="zeleno-sanduce">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-12 green-title-col ">
                        <p>Зелено сандуче</p>
                    </div>
                    <div class="col-lg-6 col-xl-4 green-info-col">
                        <h2 class="res_title"><?php the_field('zeleno_sanduce_naslov'); ?></h2>
                        <p><?php the_field('zeleno_sanduce_tekst'); ?></p>
                        <a href="<?php the_field('zeleno_sanduce_link_dugmeta'); ?>"><img src="/wp-content/uploads/2020/12/group_40.png"><span>Више информација</span></a>
                    </div>
                    <div class="col-lg-6 col-xl-3 green-tree-col">
                        <img src="/wp-content/uploads/2020/12/group_116.png" >
                    </div>
                    <div class="col-lg-12 col-xl-5 grenn-box-col">
                        <div class="red-box-wrapper"> 
                            <h2 class="res_title"><?php the_field('crvena_sekcija_naslov'); ?></h2>
                            <div class="red-box-devider"></div>
                            <div class="rb-con-wrap">
                                <p><?php the_field('crvena_sekcija_tekst'); ?></p>
                                <a href="<?php the_field('crvena_sekcija_link'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/right-arrow.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SECTION 7 END-->
    
    <!-- SECTION 8 -->
    <div class="container-fluid contact-fluid res-padding" id="kontakt">
        <div class="row">
            <div class="container custom-width-cont">
                <div class="row">
                    <div class="col-12 eservice-title-col">
                        <p>Контактирајте нас</p>
                    </div>
                    <div class="col-md-5 contact-info-col res_title">
                        <h2>контактирајте нас</h2>
                        <p>Уколико имате питање, коментар, сугестију или било какав проблем, контактирајте нас и потрудићемо се да Вам одговоримо у најкраћем року.</p>
                    </div> 
                    <div class="col-md-7 man-cform-col">
                        <div class="cform-wrapper-div">
                            <?php echo do_shortcode('[contact-form-7 id="27837" title="Kontaktirajte nas"]'); ?>
                        </div>
                    </div>      
                </div>
            </div>
        </div>
    </div>
    <!-- SECTION 8 END-->

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; 

get_footer();
?>
