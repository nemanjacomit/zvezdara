<?php
/**
 * zvezdara functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package zvezdara
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.1' );
}

if ( ! function_exists( 'zvezdara_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function zvezdara_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on zvezdara, use a find and replace
		 * to change 'zvezdara' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'zvezdara', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'glavni-meni' => esc_html__( 'Glavni meni', 'zvezdara' ),
			)
		);
		register_nav_menus(
			array(
				'sidebar-meni' => esc_html__( 'Bočna traka', 'zvezdara' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		// add_theme_support(
		// 	'custom-background',
		// 	apply_filters(
		// 		'zvezdara_custom_background_args',
		// 		array(
		// 			'default-color' => 'ffffff',
		// 			'default-image' => '',
		// 		)
		// 	)
		// );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'zvezdara_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
	function zvezdara_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'zvezdara_content_width', 640 );
}
add_action( 'after_setup_theme', 'zvezdara_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function zvezdara_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'zvezdara' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'zvezdara' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'zvezdara_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
// get random number
function wpmix_get_random() {
	$randomizr = '-' . rand(100,999);
	return $randomizr;
}
$random_number = wpmix_get_random();
global $random_number;


function register_navwalker(){
	require_once get_template_directory() . '/css/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );


function zvezdara_scripts() {
	global $random_number;
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap-4.5.3/css/bootstrap.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'zvezdara-style', get_stylesheet_uri(), array(),  _S_VERSION );
	wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/swiper/css/swiper.min.css', array(), _S_VERSION);
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/fancybox-master/dist/jquery.fancybox.min.css', array(), _S_VERSION);
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/css/custom.css', array(), $random_number);
	//wp_style_add_data( 'zvezdara-style', 'rtl', 'replace' );

	wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jQuery.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/css/bootstrap-4.5.3/js/bootstrap.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/swiper/js/swiper.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/fancybox-master/dist/jquery.fancybox.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', array(), _S_VERSION, true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		// wp_enqueue_script( 'comment-reply' );
	}

add_action( 'wp_enqueue_scripts', 'zvezdara_scripts' );

// Add image sizes
add_image_size( 'posts-size', 590, 500, true);

// Implement acf options page 
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Opcije teme',
		'menu_title'	=> 'Opcije teme',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

// Excerpt length
function custom_excerpt_length() {
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length');

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


// Footer copyright customization

function zvezdaraCustomizer($wp_customize) {

$wp_customize->add_section( 'Footer', array(
	'title' => __( 'Footer', 'zvezdara' ),
	'priority' => 200,
) );

$wp_customize->add_setting( 'zvezdara_copyright_text' );

$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer_copyright_text', array(
	'label'    => __( 'Footer copyright text', 'zvezdara' ),
	'section'  => 'Footer',
	'settings' => 'zvezdara_copyright_text',
	'type'     => 'text',
	'priority' => 200
) ) );





// Socials links
$wp_customize->add_section( 'drustvene_mreze', array(
	'title' => __( 'Drustvene mreze', 'zvezdara' ),
	'priority' => 200,
) );

$wp_customize->add_setting( 'facebook_link' );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fb_link', array(
	'label'    => __( 'Facebook link', 'zvezdara' ),
	'section'  => 'drustvene_mreze',
	'settings' => 'facebook_link',
	'type'     => 'text',
	'priority' => 220
) ) );

$wp_customize->add_setting( 'twitter_link' );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter_link', array(
	'label'    => __( 'Twitter link', 'zvezdara' ),
	'section'  => 'drustvene_mreze',
	'settings' => 'twitter_link',
	'type'     => 'text',
	'priority' => 220
) ) );
}

add_action('customize_register','zvezdaraCustomizer');

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
//require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
//require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// if ( defined( 'JETPACK__VERSION' ) ) {
// 	require get_template_directory() . '/inc/jetpack.php';
// }