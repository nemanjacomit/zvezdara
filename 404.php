<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package zvezdara
 */

get_header();
$image = get_the_post_thumbnail_url();
?>
<!-- ALL PAGES HERO SECTION -->
<div class="container-fluid all_hero_fluid" style="background-image: url('<?php if($image) {echo $image; } else { echo get_template_directory_uri() . '/images/naslovna-zvezdara-320h.jpg';  }  ?>');">
    <div class="overlay-div"></div>
    <div class="row">
        <div class="col-4 title-subtitle-wrap-sm">
            <div class="hero-title-wrapper">
                <h2>ОПШТИНА <br>ЗВЕЗДАРА</h2>
            </div>
            <div class="hero-subtitle-wrapper">
                <h3>У служби грађана</h3>
            </div>
		</div>
		<div class="col-md-4 hero-search-col">
			<div class="hero-search-wrap">
                <?php get_search_form(); ?>
                <p id="ss"></p>
            </div>
		</div>
    </div>
</div>

	<main id="primary" class="site-main">
		<div class="container-fluid fluid-404">
			<div class="contnent-404">
				<h2>ГРЕШКА!</h2>
				<p>Страна коју сте тражили не може бити приказана, Могуће је да је обрисана, преименована или привремено недоступна.</p>
				<a href="<?php echo home_url(); ?>">НАСЛОВНА СТРАНИЦА</a>
			</div>
			<div class="image-404">
				<img src="<?php echo get_template_directory_uri() . '/images/problemimage.jpg' ?>" >
			</div>
		</div>
		

	</main><!-- #main -->

<?php
get_footer();
