<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zvezdara
 */

get_header();
global $post;
$image = get_the_post_thumbnail_url();
?>
<!-- ALL PAGES HERO SECTION -->
<div class="container-fluid all_hero_fluid" style="background-image: url('<?php if($image) {echo $image; } else { echo get_template_directory_uri() . '/images/naslovna-zvezdara-320h.jpg';  }  ?>');">
    <div class="overlay-div"></div>
    <div class="row">
        <div class="col-4 title-subtitle-wrap-sm">
            <div class="hero-title-wrapper">
                <h2>ОПШТИНА <br>ЗВЕЗДАРА</h2>
            </div>
            <div class="hero-subtitle-wrapper">
                <h3>У служби грађана</h3>
            </div>
		</div>
		<div class="col-md-4 hero-search-col">
			<div class="hero-search-wrap">
                <?php get_search_form(); ?>
                <p id="ss"></p>
            </div>
		</div>
    </div>
</div>

<main id="primary" class="site-main">

	<?php
	while ( have_posts() ) :
		the_post();
		$children = get_pages( array( 'child_of' => $post->ID ) ); ?>

			<?php if ( count( $children ) > 0 ) { ?>
				<div class="container-fluid parent-page-template-wrapper">
					<div class="row">
						<div class="col-md-8 parent-col title-content-page-col">
							<div class="page_title">
								<h1><?php the_title(); ?></h1>
							</div>
							<?php 
								foreach($children as $ch_page) { ?> 
									<a class="child-link-wrapper" href="<?php the_permalink($ch_page); ?>">
										<div class="child-link">
											<p><?php echo $ch_page->post_title ?></p>
											<img src="<?php echo get_template_directory_uri()?>/images/arrow-right.png">
										</div>
									</a>
								<?php
								}
							?>
						</div>
						<div class="col-md-4">
							<?php 	
								get_template_part( 'template-parts/content', 'right_sidebar' );
							?>
						</div>
					</div>
				</div>


			<?php
			} elseif(count( $children ) < 1 && $post->post_parent == 0) { ?>
				<div class="container-fluid sample-page-template-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="page_contnet_wrapper">
								<div class="row">
									<div class="col-md-8 title-content-page-col">
										<div class="page_title">
											<h1><?php the_title(); ?></h1>
										</div>
										<div class="page_contnet">
											<?php the_content(); ?>
										</div>
									</div>
									<div class="col-md-4">
										<?php 	
											get_template_part( 'template-parts/content', 'right_sidebar' );
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }
			
			
			
			
			
			
			
			
			
			
			else { ?>
			<div class="container-fluid left-sidebar-template-wrapper">
				<div class="row">
					<?php 
						get_template_part( 'template-parts/content', 'related_pages' );
					?>
					<div class="col-md-10">
						<div class="page_contnet_wrapper">
							<div class="row">
								<div class="col-md-8 title-content-page-col">
									<div class="page_title">
										<h1><?php the_title(); ?></h1>
									</div>
									<div class="page_contnet">
										<?php the_content(); ?>
									</div>
								</div>
								<div class="col-md-4">
									<?php 	
										get_template_part( 'template-parts/content', 'right_sidebar' );
									?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
				<?php
				}
			?>
		
			
	

			
	<?php
	endwhile; // End of the loop.

	?>

	</main><!-- #main -->

<?php

get_footer();
