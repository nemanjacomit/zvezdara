// SWIPER HOME
var swiper = new Swiper('.swiper-container', {
   slidesPerView: 5,
   spaceBetween: 30,
   navigation: {
      nextEl: '.swiper-button-next-unique',
    prevEl: '.swiper-button-prev-unique'
   },
   breakpoints: {
      // when window width is <= 768px
      768: {
          slidesPerView: 1,
          spaceBetweenSlides: 30
      },
      // when window width is <= 991px
      991: {
         slidesPerView: 2,
         spaceBetweenSlides: 40
     },
     // when window width is <= 1200px
      1200: {
          slidesPerView: 3,
          spaceBetweenSlides: 40
      }
  }
});


// Search placeholder
var i = 0;
var txt = 'Како можемо да помогнемо?';
var speed = 20;

function typeWriter() {
   if(i == 0) {
      document.getElementById("s").placeholder = "";
   }
  if (i < txt.length) {
    document.getElementById("s").placeholder += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}


// handle links with @href started with '#' only
jQuery(document).ready(function ($) {

$('#menu-bocna-traka-meni').on('click', 'a[href^="#"]', function(e) {
   // target element id
   var id = $(this).attr('href');
   // target element
   var $id = $(id);
   if ($id.length === 0) {
       return;
   }
   // prevent standard hash navigation (avoid blinking in IE)
   e.preventDefault();
   // top position relative to the document
   var pos = $id.offset().top -200;
   // animated top scrolling
   $('body, html').animate({scrollTop: pos});
});


// Fancybox
$('[data-fancybox="gallery"]').fancybox({
	loop: true,
});


// Sidebar dropdown
(function($) {
   $.fn.clickToggle = function(func1, func2) {
       var funcs = [func1, func2];
       this.data('toggleclicked', 0);
       this.click(function() {
           var data = $(this).data();
           var tc = data.toggleclicked;
           $.proxy(funcs[tc], this)();
           data.toggleclicked = (tc + 1) % 2;
       });
       return this;
   };
}(jQuery));

$(document).ready(function() {
   if($(window).width() < 768) {
      $('.blue-sidebar-wrapper h3').clickToggle(function() {  
         $('.blue-sidebar-wrapper ul').slideDown();
         $('.arrow-pointert-wrapper').addClass( "open-list" );
      },
      function() {
         $('.blue-sidebar-wrapper ul').slideUp();
         $('.arrow-pointert-wrapper').removeClass( "open-list" );
      });
   }
});













$(document).on('click', '.dropdown-menu', function (e) {
   e.stopPropagation();
 });
 



//  Responsive menu
if($(window).width() < 1200) {
   $('#menu-meni a').each(function() { 
      if ($(this).find('img').length) {
         $(this).css('display', 'none'); 
      }
   });

$('#menu-meni > .menu-item-has-children > ul > .menu-item-has-children > .dropdown-toggle').each(function() {
   $(this).on('click', function(){
      $(this).next('.dropdown-menu').slideToggle( "slow" );
      var ss = $(this).find('.dropdown-menu');
   });
});


$('#menu-meni > .menu-item-has-children > .dropdown-toggle').each(function() {
   $(this).on('click', function(){
      $(this).next('.dropdown-menu').slideToggle( "slow" );
   });
});


$('#menu-meni > .menu-item-has-children > .dropdown-toggle').each(function() {
   $(this).on('click', function(){
      $( this ).parent().siblings().find('.dropdown-menu').slideUp( "slow" );
   });
   
});

$('.navbar-toggler').on('click', function(){
   $(this).next('#bs-example-navbar-collapse-1').slideToggle( "fast" );;
})





}

 });


 jQuery(document).ready(function ($) {
   if($(window).width() >= 1200) {
   var $subMenu = $('.dropdown-menu a.dropdown-toggle').next(".dropdown-menu");
   $subMenu.addClass('show');
   return false;
   }
});

