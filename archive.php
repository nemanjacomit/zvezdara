<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zvezdara
 */

get_header();
// vars
$term = get_queried_object();
global $post;
$image = get_field('slika_kategorije', $term);
?>
<!-- ALL PAGES HERO SECTION -->
<div class="container-fluid all_hero_fluid" style="background-image: url('<?php if($image) {echo $image; } else { echo get_template_directory_uri() . '/images/naslovna-zvezdara-320h.jpg';  }  ?>');">
    <div class="overlay-div"></div>
    <div class="row">
        <div class="col-12 col-lg-4 title-subtitle-wrap-sm">
            <div class="hero-title-wrapper">
                <h2>ОПШТИНА <br>ЗВЕЗДАРА</h2>
            </div>
            <div class="hero-subtitle-wrapper">
                <h3>У служби грађана</h3>
            </div>
		</div>
		<div class="col-12 col-lg-4 hero-search-col">
			<div class="hero-search-wrap">
                <?php get_search_form(); ?>
                <p id="ss"></p>
            </div>
		</div>
    </div>
</div>

<!-- CATEGORY POSTS SECTION START -->
<div class="container-fluid left-sidebar-template-wrapper">
    <div class="row">
        <div class="col-md-2">
            <div class="blue-sidebar-wrapper">
                <h3>КАТЕГОРИЈE:</h3>
                <div class="arrow-pointert-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="17.6" height="17.509" viewBox="0 0 17.6 17.509"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M26.6,13.5,17.8,31.009,9,13.5Z" transform="translate(-9 -13.5)"/></svg>
                </div>
                <ul><?php
                    $cat_id = $term->term_id;
                    $cat_title = $term->cat_name;
                    $widget_field = get_field('izaberite_kategorije', 'option');  
                        foreach($widget_field as $w_field) { 
                        $posts_count = $w_field->count;
                            if($posts_count > 0) { 
                            ?>
                                <li <?php if($w_field->term_id == $cat_id) { echo 'id="active_link" ' ; } ?> >
                                    <a href="<?php echo get_term_link($w_field); ?>"><?php echo $w_field->name; ?></a> 
                                </li>    
                            <?php
                            }
                        } 
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-md-10 ">
            <div class="main-posts-wrapper">
            <div class="row">
                <div class="col-12 sugestion-title-col">
                    <p><?php echo $term->name; ?></p>
                </div>
                <?php 
                    $currentPage = get_query_var('paged') ? get_query_var('paged') : 1;
                    $args = array(
                        'cat' => $cat_id,
                        'posts_per_page' => 12,
                        'paged' => $currentPage
                    );
                    $cats_posts = new WP_Query( $args );

                    if($cats_posts->have_posts()) :
                        while($cats_posts->have_posts()): 
                            $cats_posts->the_post(); ?>

                    <div class="col-sm-6 col-md-6 col-xl-3">
                        <div class="slider-post-wrapper">
                            <div class="slider-post-image">
                                <?php the_post_thumbnail('posts-size'); ?>
                            </div>
                            <div class="post-box-contnet-wrapper">
                                <div class="slider-post-date cat_and_date">
                                    <span><?php echo get_the_date('d.m.Y'); ?></span>
                                    <p><?php echo get_the_category()[0]->name; ?></p>
                                </div>
                                <div class="slider-post-title">
                                    <h3><?php  the_title(); ?></h3>
                                </div>
                                <div class="slider-post-button">
                                    <a href="<?php the_permalink(); ?>">Читајте даље</a>
                                </div>
                            </div>
                        </div>
                    </div>


                <?php
                endwhile;
                wp_reset_postdata();
                
            endif;
                ?>
                <div class="col-12">
                    <div class="pagination-wrapper">
                        <?php 
                            echo paginate_links(array(
                                'total' => $cats_posts->max_num_pages,
                                'prev_text'    => __('« Предходна'),
                                'next_text'    => __('Следећа »')
                            ));  
                        ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- CATEGORY POSTS SECTION END -->
<?php
get_footer();
