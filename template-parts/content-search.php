<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zvezdara
 */

global $wp_query;
$not_singular = 'Тражених резултата: ' . $wp_query->found_posts;
?>
<p class="results-count"><?php echo $not_singular; ?></p>

<?php
global $query_string;
$query_args = explode("&", $query_string);
$search_query = array(
	'posts_per_page' => -1
);

foreach($query_args as $key => $string) {
$query_split = explode("=", $string);
$search_query[$query_split[0]] = urldecode($query_split[1]);
} 

$the_query = new WP_Query($search_query);
if ( $the_query->have_posts() ) : 
?>

<ul class="s-results-list">    
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<li class="s-res-wrapper">
			<div class="s-res-title-excerpt">
				<h3><?php the_title(); ?></h3>
				<p><?php echo  get_the_excerpt(); ?></p>
			</div>
			<div class="s-res-link">
				<a href="<?php the_permalink(); ?>">Прочитај више</a>
			</div>
			
		</li>   
	<?php endwhile; ?>
</ul>

<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>




								
					
							

