<?php 
    $parent = wp_get_post_parent_id($post);
    $child_args = array(
        'post_parent' => $parent, 
        'post_type'   => 'page',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'DESC'
    );
    $pos = get_children($child_args);
?>

<div class="col-md-2">
    <div class="blue-sidebar-wrapper page_childrens_sidebar">
        <h3>
            <?php
            echo get_the_title($parent) . ":";
            ?>
        </h3>
        <div class="arrow-pointert-wrapper">
            <svg xmlns="http://www.w3.org/2000/svg" width="17.6" height="17.509" viewBox="0 0 17.6 17.509"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M26.6,13.5,17.8,31.009,9,13.5Z" transform="translate(-9 -13.5)"/></svg>
        </div>
        <ul>
          <?php 
            foreach($pos as $ch_pos) { ?>
                <li <?php if($ch_pos->ID == $post->ID) { echo 'id="active_link" ' ; } ?> >
                    <a href="<?php the_permalink($ch_pos); ?>"><?php echo $ch_pos->post_title; ?></a>
                </li>
            <?php    
            }
          ?>
            
        </ul>
    </div>
</div>







