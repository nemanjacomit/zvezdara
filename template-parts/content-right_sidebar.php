<div class="right-sidebar-wrapper">
    <div class="services-links-sidebar-wrapper"> 
        <?php
            $service = get_field('servisi za gradjane', 'option');
            foreach($service as $fl) { 
                ?> 
            <a class="s-service-wrapper" href="<?php echo $fl['link_servisa']; ?>">
                <div class="service_icon-wrapper">
                    <img src="<?php echo $fl['slika_servisa']; ?>">
                </div>
                
                <h3><?php echo $fl['naslov_servisa']; ?></h3>
            </a>
        <?php
        }
        ?>
    </div>
    <div class="baners-sidebar-wrapper">
        <?php
            $baners = get_field('baneri', 'options');
            foreach($baners as $baner) : ?>
            <div class="sidbar-baner">
                <div class="sidbar-baner_img-wrapper">
                    <img src="<?php echo $baner['slika_banera']; ?>">
                </div>
                <div class="sidebar-baner-contnet_wrapper">
                    <h3><?php echo $baner['naziv_banera']; ?></h3>
                    <div class="sb_devider"></div>
                    <a class="sb_link" href="<?php echo $baner['link_banera']; ?>">Сазнај више</a>
                </div>
            </div>
        <?php
        endforeach;	
        ?>
    </div>
</div>