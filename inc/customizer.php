<?php
/**
 * zvezdara Theme Customizer
 *
 * @package zvezdara
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function zvezdara_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'zvezdara_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'zvezdara_customize_partial_blogdescription',
			)
		);
	}
	//========
	// Logo
	//========

	// Logo Section
	$wp_customize->add_section( 'Logo', array(
		'title' => __( 'Logo', 'zvezdara' ),
		'priority' => 1,

	) );

	// Logo field
	$wp_customize->add_setting( 'logo_settings1' );

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo1', array(
		'label'      => __( 'Upload a logo', 'zvezdara' ),
		'section'    => 'Logo',
		'settings'   => 'logo_settings1',
		'context'    => 'Logo',
		'priority' => 1,
	) ) );
}
add_action( 'customize_register', 'zvezdara_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function zvezdara_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function zvezdara_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function zvezdara_customize_preview_js() {
	wp_enqueue_script( 'zvezdara-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'zvezdara_customize_preview_js' );
