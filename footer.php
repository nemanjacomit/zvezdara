<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zvezdara
 */

?>

</div><!-- #page -->

<footer id="colophon" class="site-footer">
	<div class="site-info">
		<div class="copyright-wrapper">
			<?php echo get_theme_mod('zvezdara_copyright_text'); ?>
		</div>
		<div class="footer-socials-wrapper">
			<a href="<?php echo get_theme_mod('facebook_link'); ?>">
			<svg xmlns="http://www.w3.org/2000/svg" width="15.593" height="29.114" viewBox="0 0 15.593 29.114"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M16.181,16.377l.809-5.269H11.934V7.688A2.635,2.635,0,0,1,14.9,4.842h2.3V.356A28.029,28.029,0,0,0,13.123,0C8.959,0,6.238,2.524,6.238,7.092v4.016H1.609v5.269H6.238V29.114h5.7V16.377Z" transform="translate(-1.609)"/></svg>
			</a>
			<a href="<?php echo get_theme_mod('twitter_link'); ?>">
			<svg xmlns="http://www.w3.org/2000/svg" width="29.114" height="23.646" viewBox="0 0 29.114 23.646"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M26.121,9.274c.018.259.018.517.018.776,0,7.888-6,16.977-16.977,16.977A16.862,16.862,0,0,1,0,24.348a12.344,12.344,0,0,0,1.441.074,11.95,11.95,0,0,0,7.408-2.549A5.977,5.977,0,0,1,3.27,17.734a7.525,7.525,0,0,0,1.127.092,6.311,6.311,0,0,0,1.57-.2,5.968,5.968,0,0,1-4.785-5.856v-.074a6.009,6.009,0,0,0,2.7.757,5.976,5.976,0,0,1-1.847-7.98,16.961,16.961,0,0,0,12.3,6.244,6.736,6.736,0,0,1-.148-1.367A5.973,5.973,0,0,1,24.514,5.265,11.748,11.748,0,0,0,28.3,3.824a5.951,5.951,0,0,1-2.623,3.288,11.962,11.962,0,0,0,3.436-.924,12.827,12.827,0,0,1-2.993,3.085Z" transform="translate(0 -3.381)"/></svg>
			</a>
			
		</div>
		
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>
</div>
</body>
</html>
