<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zvezdara
 */

get_header();
global $post;
$post_cat = get_the_category()[0] -> term_id;
$image = get_the_post_thumbnail_url();
?>
	<!-- ALL PAGES HERO SECTION -->
	<div class="container-fluid all_hero_fluid" style="background-image: url('<?php  echo get_template_directory_uri() . '/images/naslovna-zvezdara-320h.jpg'; ?>');">
		<div class="overlay-div"></div>
		<div class="row">
			<div class="col-4 title-subtitle-wrap-sm">
				<div class="hero-title-wrapper">
					<h2>ОПШТИНА <br>ЗВЕЗДАРА</h2>
				</div>
				<div class="hero-subtitle-wrapper">
					<h3>У служби грађана</h3>
				</div>
			</div>
			<div class="col-md-4 hero-search-col">
				<div class="hero-search-wrap">
					<?php get_search_form(); ?>
					<p id="ss"></p>
				</div>
			</div>
		</div>
	</div>


<?php
	while ( have_posts() ) :
	the_post(); ?>
	<main id="primary" class="site-main">
		<div class="container-fluid post-fluid">
			<div class="row">
				<div class="col-md-2">
					<div class="blue-sidebar-wrapper post-sidebar">
						<h3>КАТЕГОРИЈЕ:</h3>
						<ul>
						<?php
							$widget_field = get_field('izaberite_kategorije', 'option');
							
							foreach($widget_field as $w_field) { 
								$posts_count = $w_field->count;
								if($posts_count > 0) { ?>
									<li <?php if($w_field->term_id == $post_cat) { echo 'id="active_link" ' ; } ?> >
										<a href="<?php echo get_term_link($w_field); ?>"><?php echo $w_field->name; ?></a> 
									</li>    
								<?php
								}
							} 
					 	?>
						</ul>
					</div>
				</div>
				<div class="col-md-10">
					<div class="post-content-wrapper">
						<div class="row">
							<div class="col-md-8 title-content-post-col">
								<div class="post_title">
									<h1><?php the_title(); ?></h1>
									<p class="post_date"><?php the_date('d.m.Y'); ?></p>
								</div>
								<div class="post-content">

									<?php 
									$post_gallery = get_field('slike_galerije'); 
									if($post_gallery) {
										$gal_array = array_slice($post_gallery, 0, 3);
										$gal_items = count($gal_array);
										
									if($gal_items < 2) { ?>
										<div class="single_image_gal_wrapper">
											<img src="<?php echo $gal_array[0]; ?>" >
										</div>
										<?php } 
									
									elseif($gal_items == 2) { ?>
										<div class="gallery_2_images_wrapper">
											<div class="g2-col l-g2-col">
												<a data-fancybox="gallery" href="<?php echo $gal_array[0]; ?>" style="background-image: url(<?php echo $gal_array[0]; ?>);" ></a>
											</div>
											<div class="g2-col r-g2-col">
												<a data-fancybox="gallery" href="<?php echo $gal_array[1]; ?>" style="background-image: url(<?php echo $gal_array[1]; ?>);" ></a>
											</div>
										</div>
										
									<?php } 
									
									else { ?>
										<div class="gallery-post-wrapper">
											<div class="big-pic-col">
												<a data-fancybox="gallery" href="<?php echo $gal_array[0]; ?>" style="background-image: url(<?php echo $gal_array[0]; ?>);" ></a>
											</div>
											<div class="small-pic-col">
												<a data-fancybox="gallery" href="<?php echo $gal_array[1]; ?>" style="background-image: url(<?php echo $gal_array[1]; ?>);"></a>
												<a data-fancybox="gallery" href="<?php echo $gal_array[2]; ?>" style="background-image: url(<?php echo $gal_array[2]; ?>);"></a>
											</div>
											<div class="rest_images">
												<?php 
													$gal_rest = array_slice($post_gallery, 3);
													if(!empty($gal_rest)) {
														foreach ($gal_rest as $gl) { ?>
														<a data-fancybox="gallery" href="<?php echo $gl; ?>" style="background-image: url(<?php echo $gl; ?>);"></a>
													<?php }
													} 
												?>
											</div>
										</div>
									<?php 
									} 
								}
								the_content(); 
								?>
								</div>
								<div class="post_share_wrapper">
									<p>Подели:</p>
									<a class="share-link" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" title="Share on Facebook." target="_blank">
										<svg xmlns="http://www.w3.org/2000/svg" width="10.609" height="19.808" viewBox="0 0 10.609 19.808"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M11.523,11.142l.55-3.585H8.634V5.231a1.792,1.792,0,0,1,2.021-1.937h1.564V.242A19.07,19.07,0,0,0,9.443,0C6.61,0,4.758,1.717,4.758,4.825V7.557H1.609v3.585H4.758v8.666H8.634V11.142Z" transform="translate(-1.609)"/></svg>
									</a>
									<a class="share-link" href="https://twitter.com/share?url=<?php echo urlencode(get_permalink()); ?>" onclick="window.open(this.href); return false;">
										<svg xmlns="http://www.w3.org/2000/svg" width="19.808" height="16.088" viewBox="0 0 19.808 16.088"><defs><style>.a{fill:#134077;}</style></defs><path class="a" d="M17.772,7.39c.013.176.013.352.013.528A11.471,11.471,0,0,1,6.234,19.469,11.472,11.472,0,0,1,0,17.646a8.4,8.4,0,0,0,.98.05,8.13,8.13,0,0,0,5.04-1.734,4.067,4.067,0,0,1-3.8-2.815,5.12,5.12,0,0,0,.767.063,4.294,4.294,0,0,0,1.068-.138A4.06,4.06,0,0,1,.8,9.087v-.05a4.089,4.089,0,0,0,1.835.515,4.066,4.066,0,0,1-1.257-5.43A11.539,11.539,0,0,0,9.753,8.371a4.583,4.583,0,0,1-.1-.93,4.064,4.064,0,0,1,7.026-2.778,7.993,7.993,0,0,0,2.577-.98A4.049,4.049,0,0,1,17.47,5.92a8.138,8.138,0,0,0,2.338-.628,8.727,8.727,0,0,1-2.036,2.1Z" transform="translate(0 -3.381)"/></svg>
									</a>
								</div>
								<div class="row related_row">
									<?php
									$rel_posts_arr = array(
										'cat' => $post_cat,
										'posts_per_page' => 3,
										'post__not_in' => array($post->ID)
									);
									$rel_posts = get_posts($rel_posts_arr);
									foreach($rel_posts as $rp) {  ?>
										 <div class="col-md-4">
											<div class="slider-post-wrapper">
												<div class="slider-post-image">
													<?php echo get_the_post_thumbnail($rp, 'posts-size'); ?>
												</div>
												<div class="post-box-contnet-wrapper">
													<div class="slider-post-date cat_and_date">
														<span><?php echo get_the_date('d.m.Y'); ?></span>
														<p><?php echo get_the_category($rp->ID)[0]->name; ?></p>
													</div>
													<div class="slider-post-title">
														<h3><?php echo $rp->post_title ?></h3>
													</div>
													<div class="slider-post-button">
														<a href="<?php echo get_permalink($rp) ?>">Читајте даље</a>
													</div>
												</div>
											</div>
										</div>
									<?php
									}
									
									?>
								</div>
							</div>


							<div class="col-md-4">
								<?php 	
									get_template_part( 'template-parts/content', 'right_sidebar' );      
								?>
							</div>

						</div>
					</div>		
				</div>
			</div>
		</div>
	</main><!-- #main -->
			
<?php
endwhile; // End of the loop.
get_footer();



