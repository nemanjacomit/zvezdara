<?php
/**
 * @package zvezdara
 */

get_header();
?>
	<div class="container-fluid home_hero_fluid" style="background-image: url('<?php $image = get_the_post_thumbnail_url(); if($image) {echo $image; } else { echo get_template_directory_uri() . '/images/naslovna-zvezdara-320h.jpg';  }  ?>');">
		<div class="overlay-div"></div>
		<div class="row">
			<div class="col-12 title-subtitle-wrap">
				<div class="hero-title-wrapper">
					<h1>ОПШТИНА <br>ЗВЕЗДАРА</h1>
				</div>
				<div class="hero-subtitle-wrapper">
					<h2>У служби грађана</h2>
				</div>
			</div>
			<div class="col-12">
				<div class="hero-search-wrap">
					<?php get_search_form(); ?>
					<p id="ss"></p>
				</div>
			</div>
		</div>
	</div>

<main id="primary" class="site-main">

	<div class="container-fluid parent-page-template-wrapper">
		<div class="row">
			<div class="col-md-8 parent-col">
				<?php if ( have_posts() ) : 
					while ( have_posts() ) :
						the_post(); ?>

							<?php get_template_part( 'template-parts/content', 'search' ); ?>

					<?php
					endwhile;
					else :

						get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</div>
			<div class="col-md-4">
				<?php 	

					get_template_part( 'template-parts/content', 'right_sidebar' );

				?>
			</div>
		</div>
	</div>

</main><!-- #main -->

<?php
get_footer();
